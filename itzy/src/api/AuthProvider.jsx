import axios from "axios";
import React, { createContext, useEffect, useState   } from "react"
import { getToken, removeToken } from "./token";
const  AuthContext = createContext({})


export const AuthProvider = ({children }) => {
  const [isAuthenticated ,setAuthenticated ] = useState(null);
  const [ user , setUser ] = useState({})
  const [searchData , setSearchdata] = useState([])

  useEffect(() => {
    const fetchData = async () => {
      const apiUrl = 'https://dummyjson.com/auth/me';
  
      try {
        const response = await axios.get(apiUrl, {
          headers: {
            'Authorization': `Bearer ${getToken()}`,
          },
        });
        setUser(response.data);
        setAuthenticated(true);
      } catch (error) {
        removeToken();
        setAuthenticated(false);
      }
    };
  
    fetchData();
  }, []);
  

  return (
    <AuthContext.Provider value={
      {isAuthenticated , setAuthenticated , user , setUser, searchData , setSearchdata }}>
        {children}
    </AuthContext.Provider>
  )
}

export default AuthContext;