import axios from 'axios';

const fetchData = async (url, limit) => {
  try {
    const response = await axios.get(`${url}`);
    return response.data;
    
  } catch (error) {
    console.error('Error', error.message);
    throw error;
  }
};



export { fetchData };