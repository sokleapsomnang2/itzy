const saveToken = (value) => {
    localStorage.setItem('token', value);
};
  
const getToken = () => {
    return localStorage.getItem('token'); 
};
  

const removeToken = () => {
    localStorage.removeItem('token');
};
  
export  { saveToken , getToken , removeToken};