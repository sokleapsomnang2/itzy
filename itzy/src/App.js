import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import Navbar from './component/navbar';
import Home from './component/Home';
import Logins from './component/Logins';
import Layout from './layout/layout';
import Products from './component/Products';
import ProductList from './component/ProductList';

const App = () => {

  
  return (
    <>
    <Router>
    <Routes>
          <Route path="/" element={<Layout/>}>
            <Route index element={<Home/>} />
            <Route path='product' element={<Products />} />
            <Route path="product/:id" element={<ProductList />} />
            <Route path="*" element={<Navigate to="/" />}></Route>
          </Route>
          :
          <>
            <Route index path="auth/login" element={<Logins />} />
            <Route path="*" element={<Navigate to="/auth/login" />}></Route>
          </>
    </Routes>
    </Router>
    </>
  );
};

export default App;
