import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { fetchData, postData } from "../api/requestAPI";
import AuthContext from "../api/AuthProvider";

const Products = () => {
  const { searchData, setSearchdata } = useContext(AuthContext);
  const [products, setProducts] = useState([]);
  const [searchText, setSearchtext] = useState("");
  const [skip, setSkip] = useState(0);
  console.log(skip);

  useEffect(() => {
    fetchData(`https://dummyjson.com/products?limit=10&skip=${skip}`)
      .then((data) => {
        setProducts(data.products);
      })
      .catch((error) => {
        console.error("Error", error.message);
      });
  }, [skip]);

  const onSearch = (event) => {
    event.preventDefault();
    fetchData();
  };

  const handlePaginationClick = (number) => {
    const newSkip = Math.max(number * 10 - 10, 0);
    setSkip(newSkip);
    window.scrollTo(0, 0);
  };
  

  return (
    <div>
      <div className="col-sm-12 d-flex justify-content-center mt-5">
        <div style={{ width: "500px" }}>
          <form className="d-flex" role="search">
            <input
              onChange={(e) => setSearchtext(e.target.value)}
              className="form-control m-0 me-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
            />
            <button className="btn btn-outline-success" onClick={onSearch}>
              Search
            </button>
          </form>
        </div>
      </div>
      <div className="container">
        <div className="row">
          {searchData.length === 0 ? (
            products.map((product) => {
              return (
                <div key={product.id} className="col-md-4  my-5">
                  <Link className="linktoproduct" to={`${product.id} `}>
                    <div className="p-3" style={{ height: "300px" }}>
                      <div
                        className="overflow-hidden bg-white rounded-3"
                        style={{
                          boxShadow: "0px 0px 56px 0px rgba(0,0,0,0.34)",
                        }}
                      >
                        <div className="d-flex justify-content-center ">
                          <img
                            className="rounded"
                            height={220}
                            src={product.thumbnail}
                            alt=""
                          />
                        </div>
                        <h6 className="text-center py-2">{product.title}</h6>
                      </div>
                    </div>
                  </Link>
                </div>
              );
            })
          ) : searchData.total === 0 ? (
            <h1 className="mt-5 text-center">
              There is no Item that you are looking for
            </h1>
          ) : (
            searchData.products.map((product) => {
              return (
                <div key={product.id} className="col-lg-4 col-md-6 my-5">
                  <Link className="linktoproduct" to={`${product.id} `}>
                    <div className="p-3" style={{ height: "300px" }}>
                      <div
                        className="overflow-hidden"
                        style={{
                          boxShadow: "0px 0px 56px 0px rgba(0,0,0,0.34)",
                        }}
                      >
                        <div className="d-flex justify-content-center">
                          <img height={220} src={product.thumbnail} alt="" />
                        </div>
                        <h5 className="text-center">{product.title}</h5>
                      </div>
                    </div>
                  </Link>
                </div>
              );
            })
          )}
        </div>
      </div>
    </div>
  );
};

export default Products;
