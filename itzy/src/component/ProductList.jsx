import { useParams, Link } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import axios from 'axios';

const ProductDetailsPage = () => {
  const { Id: productId } = useParams();
  const [product, setProduct] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    axios
      .get(`https://dummyjson.com/products/${productId}`)
      .then((response) => {
        setProduct(response.data);
      })
      .catch((error) => {
        console.error('Error fetching data:', error);
        setError(error);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);
     

  return (
    <div style={{ backgroundColor: 'rgb(234,236,243)', paddingBottom: '100px' }}>
      {loading ? (
        <p>Loading...</p>
      ) : error ? (
        <p>Error fetching data: {error.message}</p>
      ) : (
        <>
          {/* Render product details here */}
          <h2>{product.title}</h2>
          <p>{product.description}</p>
          {/* Add other details as needed */}
        </>
      )}
    </div>
  );
  
};

export default ProductDetailsPage;
