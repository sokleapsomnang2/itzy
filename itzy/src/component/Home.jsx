import React from "react";

const Home = () => {
  return (
    <div className="hero">
      <div class="card text-white bg-dark border-0">
        <img src="/assets/1.jpg" class="card-img" alt="Background" height="700px"></img>
        <div class="card-img-overlay d-flex flex-column justify-content-center">
            <div className="container">  
          <h5 class="card-title display-3 fw-bolder mb-0">GentleMonster New Collection</h5>
          <p class="card-text lead fs-2">
            Cheack out all the TRENDS
          </p>
            </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
