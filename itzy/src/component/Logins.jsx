import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import '../App.css';

const Logins = ({ onLogin }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate();

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      navigate('/Home');
    }
    
  }, [navigate]);

  const handleLogin = () => {
    axios
      .post('https://dummyjson.com/auth/login', {
        username: username,
        password: password,
      }, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .then(response => {
        localStorage.setItem('token', response.data.token);

        const userData = response.data.user;

        onLogin(userData);
        setError('');
      })
      .catch(error => {
        setError(error.response.data.message);
      });
  };

  return (
    <div style={{
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      height: '100vh',
    }}>
      <div className='container my-5'>
      <div className='d-flex flex-column'>
      <h1>Sign In</h1>
      <form className='my-3'>
        <label htmlFor="username">Username:</label>
        <input className='border rounded p-2'type='text' placeholder='Enter your username'></input>
      </form>
      <form className='my-3'>
      <label className='my-3' htmlFor="password">Password:</label>
        <input className='border rounded p-2' type='text' placeholder='Enter your password'></input>
      </form>
      <div>
      <button className='border rounded p-2' onClick={handleLogin}>Sign In</button>
      </div>
      <p className='my-3'>
        Need an Account ? <br />
        <span className="line text-decoration-none">
          <a href="#">Sign Up</a>
        </span>
      </p>
      </div>
      </div>
      </div>
  );
};

export default Logins;